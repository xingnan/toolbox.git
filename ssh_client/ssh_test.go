package ssh_client

import (
	"fmt"
	"os"
	"path"
	"syscall"
	"testing"
	"time"
)

type WebSSHConfig struct {
	Record     bool
	RecPath    string
	RemoteAddr string
	User       string
	Password   string
	AuthModel  AuthModel
	PkPath     string
}

type WebSSH struct {
	*WebSSHConfig
}

func TestNewSSHClient(t *testing.T) {
	confing := &WebSSHConfig{
		Record:     true,
		RecPath:    "./rec/",
		RemoteAddr: "localhost:22",
		User:       "wida",
		Password:   "wida",
		AuthModel:  PASSWORD,
	}

	handle := NewWebSSH(confing)
}

func (w WebSSH) ServerConn() {

	var config *SSHClientConfig
	switch w.AuthModel {

	case PASSWORD:
		config = SSHClientConfigPassword(
			w.RemoteAddr,
			w.User,
			w.Password,
		)
	case PUBLICKEY:
		config = SSHClientConfigPulicKey(
			w.RemoteAddr,
			w.User,
			w.PkPath,
		)
	}

	client, err := NewSSHClient(config)
	if err != nil {
		fmt.Println(">>> 初始化错误 ", err)
		return
	}
	defer client.Close()

	var recorder *Recorder
	if w.Record {
		mask := syscall.Umask(0)
		defer syscall.Umask(mask)
		os.MkdirAll(w.RecPath, 0766)
		fileName := path.Join(w.RecPath, fmt.Sprintf("%s_%s_%s.cast", w.RemoteAddr, w.User, time.Now().Format("20060102_150405")))
		f, err := os.OpenFile(fileName, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0766)
		if err != nil {
			fmt.Println(">>> 创建录像文件错误 ", err)
		}
		defer f.Close()
		recorder = NewRecorder(f)
	}

}
func NewWebSSH(conf *WebSSHConfig) *WebSSH {
	return &WebSSH{
		WebSSHConfig: conf,
	}
}
