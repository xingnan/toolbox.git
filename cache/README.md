

是缓存K/V数据，类似于 memcached 适用于在单台机器上运行的应用程序。
主要优点是，具有过期时间的线程安全的“map[string]interface{}”，不需要通过网络序列化或传输其内容。

任何对象都可以存储，给定的持续时间或永久，并且缓存可以由多个协程安全使用。


并不能用作持久性数据存储，但整个缓存可以保存到文件中并从文件中加载（使用“c.Items（）”检索要序列化的项目映射，使用“NewFrom（）”从反序列化的缓存创建缓存）以快速从停机中恢复。（有关注意事项，请参阅“NewFrom（）”的文档。


### 使用

```go
import (
	"fmt"
	"gitee.com/xingnan/toolbox/cache"
	"time"
)

func main() {
	// 创建过期时间为 5 分钟的缓存，每 10 分钟清除一次过期项目
	c := cache.New(5*time.Minute, 10*time.Minute)

	// 将键“foo”的值设置为“bar”，默认过期时间
	c.Set("foo", "bar", cache.DefaultExpiration)
	
	// 将键“baz”的值设置为 42，没有过期时间（在重新设置或使用 c.Delete（“baz”）) 删除之前不会删除该项目
	c.Set("baz", 42, cache.NoExpiration)

	// 从缓存中获取与键“foo”关联的字符串
	foo, found := c.Get("foo")
	if found {
		fmt.Println(foo)
	}
	
	// 由于 Go 是静态类型的，并且缓存值可以是任何内容，因此当值传递给不采用任意类型的函数（即 interface{}）时，需要类型断言。对于只会使用一次的值执行此操作的最简单方法 - 例如对于传递给另一个函数：
	foo, found := c.Get("foo")
	if found {
		MyFunction(foo.(string))
	}
	
	// 如果该值在同一函数中多次使用，您可以改为执行以下任一操作：
	if x, found := c.Get("foo"); found {
		foo := x.(string)
		// ...
	}
	// 或
	var foo string
	if x, found := c.Get("foo"); found {
		foo = x.(string)
	}
	// ...
	// 然后 foo 可以作为字符串自由传递

	// 想要性能？设置指针！
	c.Set("foo", &MyStruct, cache.DefaultExpiration)
	if x, found := c.Get("foo"); found {
		foo := x.(*MyStruct)
			// ...
	}
}
```