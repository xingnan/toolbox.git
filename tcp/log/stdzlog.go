// Package log 主要提供相关日志记录接口
// 包括:
//		stdzlog模块， 提供全局日志方法
//		zlogger模块,  日志内部定义协议，均为对象类方法
//
// 当前文件描述:
// @Title  stdzlog.go
// @Description    包裹zlogger日志方法，提供全局方法

package log

/*
   全局默认提供一个Log对外句柄，可以直接使用API系列调用
   全局日志对象 StdLog
*/

import "os"

// StdLog 创建全局log
var StdLog = NewLog(os.Stderr, "", BitDefault)

// Flags 获取StdLog 标记位
func Flags() int {
	return StdLog.Flags()
}

// ResetFlags 设置StdLog标记位
func ResetFlags(flag int) {
	StdLog.ResetFlags(flag)
}

// AddFlag 添加flag标记
func AddFlag(flag int) {
	StdLog.AddFlag(flag)
}

// SetPrefix 设置StdLog 日志头前缀
func SetPrefix(prefix string) {
	StdLog.SetPrefix(prefix)
}

// SetLogFile 设置StdLog绑定的日志文件
func SetLogFile(fileDir string, fileName string) {
	StdLog.SetLogFile(fileDir, fileName)
}

// CloseDebug 设置关闭debug
func CloseDebug() {
	StdLog.CloseDebug()
}

// OpenDebug 设置打开debug
func OpenDebug() {
	StdLog.OpenDebug()
}

// Debugf ====> Debug <====
func Debugf(format string, v ...interface{}) {
	StdLog.Debugf(format, v...)
}

// Debug Debug
func Debug(v ...interface{}) {
	StdLog.Debug(v...)
}

// Infof ====> Info <====
func Infof(format string, v ...interface{}) {
	StdLog.Infof(format, v...)
}

// Info -
func Info(v ...interface{}) {
	StdLog.Info(v...)
}

// ====> Warn <====
func Warnf(format string, v ...interface{}) {
	StdLog.Warnf(format, v...)
}

func Warn(v ...interface{}) {
	StdLog.Warn(v...)
}

// ====> Error <====
func Errorf(format string, v ...interface{}) {
	StdLog.Errorf(format, v...)
}

func Error(v ...interface{}) {
	StdLog.Error(v...)
}

// ====> Fatal 需要终止程序 <====
func Fatalf(format string, v ...interface{}) {
	StdLog.Fatalf(format, v...)
}

func Fatal(v ...interface{}) {
	StdLog.Fatal(v...)
}

// ====> Panic  <====
func Panicf(format string, v ...interface{}) {
	StdLog.Panicf(format, v...)
}

func Panic(v ...interface{}) {
	StdLog.Panic(v...)
}

// ====> Stack  <====
func Stack(v ...interface{}) {
	StdLog.Stack(v...)
}

func init() {
	// 因为StdLog对象 对所有输出方法做了一层包裹，所以在打印调用函数的时候，比正常的logger对象多一层调用
	// 一般的Logger对象 calldDepth=2, StdLog的calldDepth=3
	StdLog.calldDepth = 3
}
