package main

import (
	"gitee.com/xingnan/toolbox/tcp/net"
)

// Server 模块的测试函数
func main() {

	/*
		服务端测试
	*/
	// 1 创建一个server 句柄 s
	// s := net.NewServer("[tcp V0.2]")

	s := net.NewServer()

	// 2 开启服务
	s.Serve()
}
