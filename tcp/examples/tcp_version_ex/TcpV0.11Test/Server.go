package main

import (
	"gitee.com/xingnan/toolbox/tcp/iface"
	"gitee.com/xingnan/toolbox/tcp/log"
	"gitee.com/xingnan/toolbox/tcp/net"
)

// ping test 自定义路由
type PingRouter struct {
	net.BaseRouter
}

// Ping Handle
func (this *PingRouter) Handle(request iface.IRequest) {

	log.Debug("Call PingRouter Handle")
	// 先读取客户端的数据，再回写ping...ping...ping
	log.Debug("recv from client : msgId=", request.GetMsgID(), ", data=", string(request.GetData()))

	err := request.GetConnection().SendBuffMsg(0, []byte("ping...ping...ping"))
	if err != nil {
		log.Error(err)
	}
}

type HelloTcpRouter struct {
	net.BaseRouter
}

// HelloRouter Handle
func (this *HelloTcpRouter) Handle(request iface.IRequest) {
	log.Debug("Call HelloTcpRouter Handle")
	// 先读取客户端的数据，再回写ping...ping...ping
	log.Debug("recv from client : msgId=", request.GetMsgID(), ", data=", string(request.GetData()))

	err := request.GetConnection().SendBuffMsg(1, []byte("Hello tcp Router V0.10"))
	if err != nil {
		log.Error(err)
	}
}

// 创建连接的时候执行
func DoConnectionBegin(conn iface.IConnection) {
	log.Debug("DoConnecionBegin is Called ... ")

	// 设置两个链接属性，在连接创建之后
	log.Debug("Set conn Name, Home done!")
	conn.SetProperty("Name", "baidu")
	conn.SetProperty("Home", "https://www.baidu.com")

	err := conn.SendMsg(2, []byte("DoConnection BEGIN..."))
	if err != nil {
		log.Error(err)
	}
}

// 连接断开的时候执行
func DoConnectionLost(conn iface.IConnection) {
	// 在连接销毁之前，查询conn的Name，Home属性
	if name, err := conn.GetProperty("Name"); err == nil {
		log.Error("Conn Property Name = ", name)
	}

	if home, err := conn.GetProperty("Home"); err == nil {
		log.Error("Conn Property Home = ", home)
	}

	log.Debug("DoConneciotnLost is Called ... ")
}

func main() {
	// 创建一个server句柄
	s := net.NewServer()

	// 注册链接hook回调函数
	s.SetOnConnStart(DoConnectionBegin)
	s.SetOnConnStop(DoConnectionLost)

	// 配置路由
	s.AddRouter(0, &PingRouter{})
	s.AddRouter(1, &HelloTcpRouter{})

	// 开启服务
	s.Serve()
}
