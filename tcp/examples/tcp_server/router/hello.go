package router

import (
	"gitee.com/xingnan/toolbox/tcp/iface"
	"gitee.com/xingnan/toolbox/tcp/log"
	"gitee.com/xingnan/toolbox/tcp/net"
)

type HelloRouter struct {
	net.BaseRouter
}

// HelloRouter Handle
func (this *HelloRouter) Handle(request iface.IRequest) {
	log.Debug("Call HelloRouter Handle")
	// 先读取客户端的数据，再回写ping...ping...ping
	log.Debug("recv from client : msgId=", request.GetMsgID(), ", data=", string(request.GetData()))

	err := request.GetConnection().SendBuffMsg(1, []byte("Hello Router V0.10"))
	if err != nil {
		log.Error(err)
	}
}
