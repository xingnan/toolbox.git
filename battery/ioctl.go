// +build freebsd dragonfly netbsd

package battery

import (
	"unsafe"

	"golang.org/x/sys/unix"
)

func ioctl(fd int, nr int64, typ byte, size uintptr, retptr unsafe.Pointer) error {
	_, _, errno := unix.Syscall(
		unix.SYS_IOCTL,
		uintptr(fd),
		// Some magicks derived from sys/ioccom.h.
		uintptr((0x40000000|0x80000000)|
			((int64(size)&(1<<13-1))<<16)|
			(int64(typ)<<8)|
			nr,
		),
		uintptr(retptr),
	)
	if errno != 0 {
		return errno
	}
	return nil
}
