package battery

import (
	"math"
	"os/exec"

	"howett.net/plist"
)

type battery struct {
	Voltage           int
	CurrentCapacity   int `plist:"AppleRawCurrentCapacity"`
	MaxCapacity       int `plist:"AppleRawMaxCapacity"`
	DesignCapacity    int
	Amperage          int64
	FullyCharged      bool
	IsCharging        bool
	ExternalConnected bool
}

func readBatteries() ([]*battery, error) {
	out, err := exec.Command("ioreg", "-n", "AppleSmartBattery", "-r", "-a").Output()
	if err != nil {
		return nil, err
	}

	if len(out) == 0 {
		// No batteries.
		return nil, nil
	}

	var data []*battery
	if _, err = plist.Unmarshal(out, &data); err != nil {
		return nil, err
	}
	return data, nil
}

func convertBattery(battery *battery) *Battery {
	volts := float64(battery.Voltage) / 1000
	b := &Battery{
		Current:       float64(battery.CurrentCapacity) * volts,
		Full:          float64(battery.MaxCapacity) * volts,
		Design:        float64(battery.DesignCapacity) * volts,
		ChargeRate:    math.Abs(float64(battery.Amperage)) * volts,
		Voltage:       volts,
		DesignVoltage: volts,
	}
	switch {
	case !battery.ExternalConnected:
		b.State, _ = newState("Discharging")
	case battery.IsCharging:
		b.State, _ = newState("Charging")
	case battery.CurrentCapacity == 0:
		b.State, _ = newState("Empty")
	case battery.FullyCharged:
		b.State, _ = newState("Full")
	default:
		b.State, _ = newState("Unknown")
	}
	return b
}

func systemGet(idx int) (*Battery, error) {
	batteries, err := readBatteries()
	if err != nil {
		return nil, err
	}

	if idx >= len(batteries) {
		return nil, ErrNotFound
	}
	return convertBattery(batteries[idx]), nil
}

func systemGetAll() ([]*Battery, error) {
	_batteries, err := readBatteries()
	if err != nil {
		return nil, err
	}

	batteries := make([]*Battery, len(_batteries))
	for i, battery := range _batteries {
		batteries[i] = convertBattery(battery)
	}
	return batteries, nil
}
