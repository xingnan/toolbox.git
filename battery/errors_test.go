package battery

import (
	"errors"
	"testing"
)

func TestErrPartial(t *testing.T) {
	cases := []struct {
		in    ErrPartial
		str   string
		isnil bool
		nonil bool
	}{
		{ErrPartial{}, "{}", true, false},
		{ErrPartial{Full: errors.New("t1")}, "{Full:t1}", false, false},
		{ErrPartial{State: errors.New("t2"), Full: errors.New("t3")}, "{State:t2 Full:t3}", false, false},
		{ErrPartial{State: errors.New("t4"), Current: errors.New("t5"), Full: errors.New("t6"), Design: errors.New("t7"), ChargeRate: errors.New("t8"), Voltage: errors.New("t9"), DesignVoltage: errors.New("t10")}, "{State:t4 Current:t5 Full:t6 Design:t7 ChargeRate:t8 Voltage:t9 DesignVoltage:t10}", false, true},
	}

	for i, c := range cases {
		str := c.in.Error()
		isnil := c.in.isNil()
		nonil := c.in.noNil()

		if str != c.str {
			t.Errorf("%d: %v != %v", i, str, c.str)
		}
		if isnil != c.isnil {
			t.Errorf("%d: %v != %v", i, isnil, c.isnil)
		}
		if nonil != c.nonil {
			t.Errorf("%d: %v != %v", i, nonil, c.nonil)
		}
	}
}

func TestErrors(t *testing.T) {
	cases := []struct {
		in  Errors
		str string
	}{
		{Errors{nil}, "[]"},
		{Errors{ErrPartial{}}, "[{}]"},
		{Errors{ErrFatal{errors.New("t1")}}, "[Could not retrieve battery info: `t1`]"},
		{Errors{ErrPartial{Full: errors.New("t2")}, ErrFatal{errors.New("t3")}}, "[{Full:t2} Could not retrieve battery info: `t3`]"},
		{Errors{ErrPartial{Full: errors.New("t4")}, ErrPartial{Current: errors.New("t5")}}, "[{Full:t4} {Current:t5}]"},
	}

	for i, c := range cases {
		str := c.in.Error()

		if str != c.str {
			t.Errorf("%d: %v != %v", i, str, c.str)
		}
	}
}
