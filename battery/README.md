battery
=======

跨平台、标准化电池信息库。

允许访问独立于系统的、类型化的电池状态、容量、充电和电压值，根据需要重新计算，以mW、mWh或V单位返回。

当前支持的系统:

* Linux 2.6.39+
* OS X 10.10+
* Windows XP+
* FreeBSD
* DragonFlyBSD
* NetBSD
* OpenBSD
* Solaris

代码示例
------------

```go
package main

import (
	"fmt"

	"gitee.com/xingnan/toolbox/battery"
)

func main() {
	batteries, err := battery.GetAll()
	if err != nil {
		fmt.Println("Could not get battery info!")
		return
	}
	for i, battery := range batteries {
		fmt.Printf("Bat%d: ", i)
		fmt.Printf("state: %s, ", battery.State.String())
		fmt.Printf("current capacity: %f mWh, ", battery.Current)
		fmt.Printf("last full capacity: %f mWh, ", battery.Full)
		fmt.Printf("design capacity: %f mWh, ", battery.Design)
		fmt.Printf("charge rate: %f mW, ", battery.ChargeRate)
		fmt.Printf("voltage: %f V, ", battery.Voltage)
		fmt.Printf("design voltage: %f V\n", battery.DesignVoltage)
	}
}
```

*使用*

```bash
$ battery
BAT0: Full, 95.61% [Voltage: 12.15V (design: 12.15V)]
```
