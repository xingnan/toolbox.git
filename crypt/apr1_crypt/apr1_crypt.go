/**
 * @Author      : XingNan
 * @Email       : 457415936@qq.com
 * @Time        : 2021/3/8 下午5:33
 * @File        : apr1_crypt
 * @Project     : GoLand
 * @Description : 文件描述
 */
package apr1_crypt

import (
	"gitee.com/xingnan/toolbox/crypt"
	"gitee.com/xingnan/toolbox/crypt/common"
	"gitee.com/xingnan/toolbox/crypt/md5_crypt"
)

func init() {
	crypt.RegisterCrypt(crypt.APR1, New, MagicPrefix)
}

const (
	MagicPrefix   = "$apr1$"
	SaltLenMin    = 1
	SaltLenMax    = 8
	RoundsDefault = 1000
)

// New returns a new crypt.Crypter computing the variant "apr1" of MD5-crypt
func New() crypt.Crypter {
	crypter := md5_crypt.New()
	crypter.SetSalt(common.Salt{
		MagicPrefix:   []byte(MagicPrefix),
		SaltLenMin:    SaltLenMin,
		SaltLenMax:    SaltLenMax,
		RoundsDefault: RoundsDefault,
	})
	return crypter
}
