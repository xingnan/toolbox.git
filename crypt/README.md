
Usage
-----

```

    package main

    import (
    	"fmt"

    	"github.com/GehirnInc/crypt"
    	_ "github.com/GehirnInc/crypt/sha256_crypt"
    )

    func main() {
    	crypt := crypt.SHA256.New()
    	ret, _ := crypt.Generate([]byte("secret"), []byte("$5$salt"))
    	fmt.Println(ret)

    	err := crypt.Verify(ret, []byte("secret"))
    	fmt.Println(err)

    	// Output:
    	// $5$salt$kpa26zwgX83BPSR8d7w93OIXbFt/d3UOTZaAu5vsTM6
    	// <nil>
    }
```
