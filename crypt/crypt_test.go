/**
 * @Author      : XingNan
 * @Email       : 457415936@qq.com
 * @Time        : 2021/3/8 下午5:25
 * @File        : crypt_test
 * @Project     : GoLand
 * @Description : 文件描述
 */
package crypt_test

import (
	"testing"

	"gitee.com/xingnan/toolbox/crypt"
	_ "gitee.com/xingnan/toolbox/crypt/apr1_crypt"
	"github.com/stretchr/testify/assert"
)

func TestIsHashSupported(t *testing.T) {
	apr1 := crypt.IsHashSupported("$apr1$salt$hash")
	assert.True(t, apr1)
	other := crypt.IsHashSupported("$unknown$salt$hash")
	assert.False(t, other)
}
