# otp: Go / Golang 一次性密码实用程序

# 为什么要使用一次密码？

一次性密码（OTP）是一种可以仅通过密码来提高安全性的机制。当基于时间的OTP（TOTP）存储在用户的电话上，并与用户知道的内容（密码）结合使用时，您就可以轻松进行[多因素身份验证]（http：en.wikipedia.orgwikiMulti- factor_authentication），而无需添加对SMS提供程序的依赖关系。许多流行的网站（包括Google，Github，Facebook，Salesforce等）都使用此密码和TOTP组合。

otp库使您可以轻松地将TOTP添加到自己的应用程序中，从而提高用户的安全性，以防止大规模密码泄露和恶意软件。

TOTP是标准化的并且被广泛部署，所以有很多 [mobile clients and software implementations](http://en.wikipedia.org/wiki/Time-based_One-time_Password_Algorithm#Client_implementations).

## `otp` 支持/特性:

* 生成QR Code图像以方便用户注册。
* 基于时间的一次性密码算法（TOTP）（RFC 6238）：基于时间的OTP，这是最常用的方法。
* 基于HMAC的一次性密码算法（HOTP）（RFC 4226）：TOTP所基于的基于计数器的OTP。
* 两种算法的代码生成和验证。

## 应用程序中实现TOTP：

### 用户注册

有关工作注册工作流程的示例 [Github has documented theirs](https://help.github.com/articles/configuring-two-factor-authentication-via-a-totp-mobile-app/
):

1. 为用户生成新的TOTP密钥。`key,_ := totp.Generate(...)`.
1. 为用户显示密钥的密文和QR码。 `key.Secret()` 和 `key.Image(...)`.
1. 测试用户可以成功使用其TOTP。`totp.Validate(...)`.
1. 在您的后端中为用户存储TOTP 密钥。 `key.Secret()`
1. 向用户提供“恢复码”。 (参见下面的恢复代码)

### 代码生成

* 在`TOTP`或`HOTP`情况下，都可以使用`GenerateCode`函数和一个计数器或`time.Time`结构来生成与大多数实现兼容的有效代码。
* 对于不常见或自定义的设置，或捕获不太可能的错误，请在任一模块中使用`GenerateCodeCustom`。

### 验证

1. 正常提示和验证用户密码。
1. 如果用户启用了TOTP，提示请输入TOTP密码。
1. 从您的后端检索用户的TOTP密钥。
1. 验证用户的密码。`totp.Validate(...)`


### 恢复码

当用户无法访问其TOTP设备时，他们将再也无法访问其帐户。因为TOTP通常配置在可能丢失，被盗或损坏的移动设备上，所以这是一个普遍的问题。因此，许多提供商将其“备份代码”或“恢复代码”提供给其用户。这些是一组一次性使用的代码，可以代替TOTP使用。这些可以只是存储在后端中的随机生成的字符串。  [Github's documentation provides an overview of the user experience](
https://help.github.com/articles/downloading-your-two-factor-authentication-recovery-codes/).

