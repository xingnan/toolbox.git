package interop_test

import (
	"testing"
	"time"

	"gitee.com/xingnan/toolbox/otp"
	"gitee.com/xingnan/toolbox/otp/totp"
	"github.com/gokyle/twofactor"
	"github.com/stretchr/testify/require"
)

func TestTwoFactor(t *testing.T) {
	key, err := totp.Generate(totp.GenerateOpts{
		Issuer:      "Example.com",
		AccountName: "alice@example.com",
		Algorithm:   otp.AlgorithmSHA512,
	})
	require.NoError(t, err)
	require.NotNil(t, key)

	tf, label, err := twofactor.FromURL(key.URL())
	require.NoError(t, err)
	require.NotNil(t, tf)
	require.Equal(t, "Example.com:alice@example.com", label)

	code := tf.OTP()
	require.NotEmpty(t, code)

	valid, err := totp.ValidateCustom(code, key.Secret(),
		time.Now().UTC(),
		totp.ValidateOpts{
			Period:    30,
			Skew:      1,
			Digits:    otp.DigitsSix,
			Algorithm: otp.AlgorithmSHA512,
		},
	)
	require.NoError(t, err)
	require.True(t, valid)
}
