// 用于处理字节的帮助程序
package com

import (
	"unsafe"
)

// Cut 给定范围内的切片元素
func Cut(a []byte, from, to int) []byte {
	copy(a[from:], a[to:])
	a = a[:len(a)-to+from]

	return a
}

// Insert 在指定位置新建切片
func Insert(a []byte, i int, b []byte) []byte {
	a = append(a, make([]byte, len(b))...)
	copy(a[i+len(b):], a[i:])
	copy(a[i:i+len(b)], b)

	return a
}

// Replace 函数与字节不同。替换指定范围
func Replace(a []byte, from, to int, new []byte) []byte {
	lenDiff := len(new) - (to - from)

	if lenDiff > 0 {
		// Extend if new segment bigger
		a = append(a, make([]byte, lenDiff)...)
		copy(a[to+lenDiff:], a[to:])
		copy(a[from:from+len(new)], new)

		return a
	}

	if lenDiff < 0 {
		copy(a[from:], new)
		copy(a[from+len(new):], a[to:])
		return a[:len(a)+lenDiff]
	}

	// same size
	copy(a[from:], new)
	return a
}

// SliceToString 适用于大型有效载荷（零分配和更快）
func SliceToString(buf []byte) string {
	return *(*string)(unsafe.Pointer(&buf))
}