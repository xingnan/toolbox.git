/**
 * @Author      : xingnan
 * @Email       : 457415936@qq.com
 * @Time        : 16:52
 * @File        : env.go
 * @Project     : toolbox
 * @Description : 环境变量
 */
package com

import (
	"errors"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
)

// Derivator expand env engine
type Derivator struct {
	envBlock map[string]string
	mu       sync.RWMutex
}

// NewDerivator create env derivative
func NewDerivator() *Derivator {
	de := &Derivator{
		envBlock: make(map[string]string),
	}
	appdir := AppDir()
	if appdir != "." {
		de.envBlock["APPDIR"] = appdir
	}
	return de
}

func AppDir() string {
	exe, err := os.Executable()
	if err != nil {
		return "."
	}
	exedir := filepath.Dir(exe)
	if filepath.Base(exedir) == "bin" {
		return filepath.Dir(exedir)
	}
	return exedir
}

// AddBashCompatible $0~$9
func (de *Derivator) AddBashCompatible() {
	de.mu.Lock()
	defer de.mu.Unlock()
	for i := 0; i < len(os.Args); i++ {
		de.envBlock[strconv.Itoa(i)] = os.Args[i]
	}
	de.envBlock["$"] = strconv.Itoa(os.Getpid())
}

// 附加env
func (de *Derivator) Append(k, v string) error {
	if k == "" || v == "" {
		return errors.New("empty env k/v input")
	}
	de.mu.Lock()
	defer de.mu.Unlock()
	de.envBlock[k] = v
	return nil
}

// Environ create new environ block
func (de *Derivator) Environ() []string {
	de.mu.RLock()
	defer de.mu.RUnlock()
	oe := os.Environ()
	ev := make([]string, 0, len(oe)+len(de.envBlock))
	for _, e := range oe {
		kv := strings.Split(e, "=")
		if len(kv) > 0 {
			k := kv[0]
			if _, ok := de.envBlock[k]; ok {
				continue
			}
		}
		ev = append(ev, e)
	}
	for k, v := range de.envBlock {
		ev = append(ev, StrCat(k, "=", v))
	}
	return ev
}

// 删除env k
func (de *Derivator) EraseEnv(k string) {
	de.mu.Lock()
	defer de.mu.Unlock()
	delete(de.envBlock, k)
}

// 获取env k
func (de *Derivator) GetEnv(k string) string {
	de.mu.RLock()
	defer de.mu.RUnlock()
	if v, ok := de.envBlock[k]; ok {
		return v
	}
	return os.Getenv(k)
}

// 展开env
func (de *Derivator) ExpandEnv(s string) string {
	return os.Expand(s, de.GetEnv)
}
