## Supported hashing algorithms:

- apr1 (do not use except for legacy support situations)
- sha (do not use except for legacy support situations)
- bcrypt

## This is what you can

Set user credentials in a htpasswd file:

```Go
file := "/tmp/demo.htpasswd"
name := "joe"
password := "secret"
err := htpasswd.SetPassword(file, name, password, htpasswd.HashBCrypt)
```

Remove a user:

```Go
err := htpasswd.RemoveUser(file, name)
```

Read user hash table:

```Go
passwords, err := htpasswd.ParseHtpasswdFile(file)
```

Have fun.