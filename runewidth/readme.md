runewidth
============

获取字符或字符串的固定宽度

Usage
-----

```go
runewidth.StringWidth("中华人民共和国国歌")
```

