// //
// @Author      : xingnan
// @Organization:
// @Email       : xingnan@oj-time.cn
// @Time        : 2022/2/25 09:45
// @Dir         : runewidth
// @File        : runewidth_appengine.go
// @Project     : GoLand
// @Description : GoLand
// //

// +build appengine

package runewidth

// IsEastAsian return true if the current locale is CJK
func IsEastAsian() bool {
	return false
}
