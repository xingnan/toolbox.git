## logs
logs is a Go logs manager. It can use many logs adapters. The repo is inspired by `database/sql` .


## 安装

	go get gitee.com/xingnan/toolbox/logs


支持console/file/smtp/conn.

## 使用

导入

```golang
import (
	"gitee.com/xingnan/toolbox/logs"
)
```

初始化（console）

```golang
log := logs.NewLogger(10000)
log.SetLogger("console", "")
```

> 第一个参数代表多少个channel


```golang
log.Trace("trace")
log.Info("info")
log.Warn("warning")
log.Debug("debug")
log.Critical("critical")
```

## 保存文件

配置示例：

```golang
log := NewLogger(10000)
log.SetLogger("file", `{"filename":"test.log"}`)
```

## 网络传输

配置示例：

```golang
log := NewLogger(1000)
log.SetLogger("conn", `{"net":"tcp","addr":":7020"}`)
log.Info("info")
```

## Smtp传输

配置示例：

```golang
log := NewLogger(10000)
log.SetLogger("smtp", `{"username":"gotest@gmail.com","password":"xxxxxxxx","host":"smtp.gmail.com:587","sendTos":["test@gmail.com"]}`)
log.Critical("sendmail critical")
time.Sleep(time.Second * 30)
```
