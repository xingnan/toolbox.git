package logs

import (
	"testing"
	"time"
)

func TestSmtp(t *testing.T) {
	log := NewLogger(10000)
	log.SetLogger("smtp", `{"username":"gotest@gmail.com","password":"xxxxxxxx","host":"smtp.gmail.com:587","sendTos":["test@gmail.com"]}`)
	log.Critical("sendmail critical")
	time.Sleep(time.Second * 30)
}
