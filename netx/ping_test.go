package netx

import (
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestPingerAddIPs(t *testing.T) {
	pinger := &Pinger{}
	testAddIPs(t, pinger)
}

func TestPingerPing(t *testing.T) {
	if skipRawSocketTests() {
		t.Skip("Test skipped due to non-root")
	}

	pr, err := Ping("127.0.0.1")
	require.NoError(t, err)
	fmt.Printf("%v\n", pr)
}

func TestPingerStartOnce(t *testing.T) {
	if skipRawSocketTests() {
		t.Skip("Test skipped due to non-root")
	}

	ips := []string{"127.0.0.1"}

	pinger := &Pinger{}
	pinger.AddIPs(ips)

	done := make(chan struct{})
	cnt := 0

	res, err := pinger.Start()
	require.NoError(t, err)
	require.NotNil(t, res)

	go func() {
		for pr := range res {
			fmt.Printf("%v\n", pr)
			cnt++
		}
		close(done)
	}()

	select {
	case <-time.Tick(time.Duration(len(pinger.IPs())) * time.Second):
		require.FailNow(t, "Test timed out")

	case <-done:
		require.Equal(t, len(pinger.IPs()), cnt)
	}

	pinger.Stop()
}

func TestPingerStartMultiple(t *testing.T) {
	if skipRawSocketTests() {
		t.Skip("Test skipped due to non-root")
	}

	pinger := &Pinger{}
	pinger.AddIPs([]string{"127.0.0.1"})

	for i := 0; i < 5; i++ {
		done := make(chan struct{})
		cnt := 0

		res, err := pinger.Start()
		require.NoError(t, err)
		require.NotNil(t, res)

		go func() {
			for pr := range res {
				fmt.Printf("%v\n", pr)
				cnt++
			}
			close(done)
		}()

		select {
		case <-time.Tick(time.Duration(len(pinger.IPs())) * time.Second):
			require.FailNow(t, "Test timed out")

		case <-done:
			require.Equal(t, len(pinger.IPs()), cnt)
		}

		pinger.Stop()
	}
}

func TestPingerSetDF(t *testing.T) {
	if skipRawSocketTests() {
		t.Skip("Test skipped due to non-root")
	}

	pinger := &Pinger{
		DF:   true,
		Size: 1800,
	}
	pinger.AddIPs([]string{"8.8.8.8"})

	res, err := pinger.Start()
	require.NoError(t, err)
	require.NotNil(t, res)

	go func() {
		for pr := range res {
			fmt.Printf("%v\n", pr)
		}
	}()

	select {
	case <-time.Tick(time.Duration(len(pinger.IPs())) * time.Second):
	}

	pinger.Stop()
}

func skipRawSocketTests() bool {
	if os.Getuid() != 0 {
		return true
	}

	return false
}
