/**
 * @Author      : xingnan
 * @Email       : 457415936@qq.com
 * @Time        : 16:40
 * @File        : conversion.go
 * @Project     : toolbox
 * @Description : 计量单位
 */
package com

// Conversion
const (
	Kilo int64 = 1024
	KB   int64 = Kilo
	MB   int64 = KB * Kilo
	GB   int64 = MB * Kilo
	TB   int64 = GB * Kilo
	PB   int64 = TB * Kilo
	EB   int64 = PB * Kilo
	//ZB   int128= EB * Kilo

	// MAX
	UINT64MAX uint64 = ^uint64(0)
	INT64MAX  int64  = int64(^uint64(0) >> 1)
	INT64MIN  int64  = -int64(^uint64(0) >> 1)
	UINT32MAX uint32 = ^uint32(0)
	INT32MAX  int32  = int32(^uint32(0) >> 1)
	INT32MIN  int32  = -int32(^uint32(0) >> 1)
	UINT16MAX uint16 = ^uint16(0)
	INT16MAX  int16  = int16(^uint16(0) >> 1)
	INT16MIN  int16  = -int16(^uint16(0) >> 1)
	UINT8MAX  uint8  = ^uint8(0)
	INT8MAX   int8   = int8(^uint8(0) >> 1)
	INT8MIN   int8   = -int8(^uint8(0) >> 1)
)

// Maximum64 todo
func Maximum64(x, y int64) int64 {
	if x > y {
		return x
	}
	return y
}

// Minimum64 todo
func Minimum64(x, y int64) int64 {
	if x > y {
		return y
	}
	return x
}
