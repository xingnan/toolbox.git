module gitee.com/xingnan/toolbox

go 1.22.2

require (
	gitee.com/xingnan/toolbox v0.1.81
	github.com/GehirnInc/crypt v0.0.0-20230320061759-8cc1b52080c5
	github.com/adamzy/cedar-go v0.0.0-20170805034717-80a9c64b256d
	github.com/astaxie/beego v1.12.3
	github.com/boombuler/barcode v1.0.1
	github.com/cloudflare/golz4 v0.0.0-20150217214814-ef862a3cdc58
	github.com/elastic/go-elasticsearch/v6 v6.8.10
	github.com/go-sql-driver/mysql v1.8.1
	github.com/gogo/protobuf v1.3.2
	github.com/golang/protobuf v1.5.4
	github.com/hashicorp/golang-lru v1.0.2
	github.com/issue9/assert v1.5.0
	github.com/lib/pq v1.10.9
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/oschwald/maxminddb-golang v1.12.0
	github.com/pkg/errors v0.9.1
	github.com/shiena/ansicolor v0.0.0-20230509054315-a9deabde6e02
	github.com/stretchr/testify v1.9.0
	github.com/yinheli/mahonia v0.0.0-20131226213531-0eef680515cc
	golang.org/x/crypto v0.22.0
	golang.org/x/net v0.24.0
	golang.org/x/sys v0.19.0
)

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	google.golang.org/protobuf v1.33.0 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
