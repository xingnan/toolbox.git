// 通过 SSH 复制文件
package scp

import (
	"time"

	"golang.org/x/crypto/ssh"
)

// 使用提供的host和 ssh.clientConfig 返回一个新的 scp.Client 一分钟的默认超时。
func NewClient(host string, config *ssh.ClientConfig) Client {
	return NewConfigurer(host, config).Create()
}

// 返回一个新的 scp.Client 并提供host、ssh.ClientConfig 和timeout
func NewClientWithTimeout(host string, config *ssh.ClientConfig, timeout time.Duration) Client {
	return NewConfigurer(host, config).Timeout(timeout).Create()
}

// 使用已经建立的 SSH 连接返回一个新的 scp.Client
func NewClientBySSH(ssh *ssh.Client) (Client, error) {
	session, err := ssh.NewSession()
	if err != nil {
		return Client{}, err
	}
	return NewConfigurer("", nil).Session(session).Create(), nil
}

// 与 NewClientWithTimeout 相同，使用现有的 ssh连接
func NewClientBySSHWithTimeout(ssh *ssh.Client, timeout time.Duration) (Client, error) {
	session, err := ssh.NewSession()
	if err != nil {
		return Client{}, err
	}
	return NewConfigurer("", nil).Session(session).Timeout(timeout).Create(), nil
}
