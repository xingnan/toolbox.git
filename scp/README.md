scp复制文件
=============================



### 示例


```go
package main

import (
	"fmt"
	scp "gitee.com/xingnan/toolbox/scp"
	"gitee.com/xingnan/toolbox/scp/auth"
	"golang.org/x/crypto/ssh"
	"os"
)

func main() {
	// 使用密钥方式连接
	clientConfig, _ := auth.PrivateKey("username", "/path/to/rsa/key", ssh.InsecureIgnoreHostKey())

	// 对于其他身份验证方法，请参阅 ssh.ClientConfig 和 ssh.AuthMethod

	// 创建新的scp client
	client := scp.NewClient("example.com:22", &clientConfig)

	// 连接到远程服务器
	err := client.Connect()
	if err != nil {
		fmt.Println("无法与远程服务器建立连接", err)
		return
	}

	// 打开文件
	f, _ := os.Open("/path/to/local/file")

	// 复制文件后关闭客户端连接
	defer client.Close()

	// 复制文件后关闭文件
	defer f.Close()
	
	// 用法: CopyFile(fileReader, remotePath, permission)

	err = client.CopyFile(f, "/home/server/test.txt", "0655")

	if err != nil {
		fmt.Println("复制文件时出错", err)
	}
}
```

#### 使用现有SSH连接

如果有已建立的SSH连接，则可以改用已建立的连接。

```go
func connectSSH() *ssh.Client {
   // 设置SSH连接
}

func main() {
   sshClient := connectSSH()

   // 创建一个新的 SCP Client，注意这个函数可能会返回一个错误，因为一个新的 SSH 会话是使用现有的连接建立的

   client, err := scp.NewClientBySSH(sshClient)
   if err != nil {
      fmt.Println("从现有连接创建新 SSH 会话时出错", err)
   }

   /* .. 同上 .. */
}
```

#### 从远程服务器复制文件

也可以使用此库复制远程文件。
用法类似于顶部的示例，不同之处在于需要使用`CopyFromRemote`。

有关更全面的示例，请参阅 `testsbasic_test.go` 文件中的 `TestDownloadFile` 函数。


