package scp

import (
	"time"

	"golang.org/x/crypto/ssh"
)

// 包含 scp 客户端使用的所有配置选项的结构。
type ClientConfigurer struct {
	host         string
	clientConfig *ssh.ClientConfig
	session      *ssh.Session
	timeout      time.Duration
	remoteBinary string
}

// 创建新的客户端配置
// 必需的参数：host 和 ssh.ClientConfig 并返回一个配置器，其中填充了可选参数的默认值。
// 可以使用 ClientConfigurer 结构上提供的方法设置可选参数。
func NewConfigurer(host string, config *ssh.ClientConfig) *ClientConfigurer {
	return &ClientConfigurer{
		host:         host,
		clientConfig: config,
		timeout:      time.Minute,
		remoteBinary: "scp",
	}
}

// 设置scp二进制文件的路径
// 默认：/usr/bin/scp
func (c *ClientConfigurer) RemoteBinary(path string) *ClientConfigurer {
	c.remoteBinary = path
	return c
}

// 设置主机
func (c *ClientConfigurer) Host(host string) *ClientConfigurer {
	c.host = host
	return c
}

// 设置超时时间
// 默认60秒
func (c *ClientConfigurer) Timeout(timeout time.Duration) *ClientConfigurer {
	c.timeout = timeout
	return c
}

// 设置 ssh.ClientConfig
func (c *ClientConfigurer) ClientConfig(config *ssh.ClientConfig) *ClientConfigurer {
	c.clientConfig = config
	return c
}

// 设置 ssh.Session
func (c *ClientConfigurer) Session(session *ssh.Session) *ClientConfigurer {
	c.session = session
	return c
}

// 使用ClientConfigurer创建Client
func (c *ClientConfigurer) Create() Client {
	return Client{
		Host:         c.host,
		ClientConfig: c.clientConfig,
		Timeout:      c.timeout,
		RemoteBinary: c.remoteBinary,
		Session:      c.session,
	}
}
